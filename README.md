# Experiments with Ruby Ractors

This is a list of experiments with Ruby 3's Ractors, in order of growing complexity. The final one (`ractor-demo-9.rb`) implements a class and a module that could be used is a real program to run code inside ractors without having to explicitly create them.

Developed and run on Ubuntu 20.04 with Ruby 3.0.0.

# Setup

Install Ruby 3, for example with

`rvm ruby-3.0.0`
`rvm use ruby-3.0.0`

# List of the experiments

* `ractor-demo-01.rb` starts one ractor per processor and makes them run a busy loop. Sleep for 10 seconds and terminate the ractors. You have time to use `htop` and check that Ruby fills all the cores of your computer.

* `ractor-demo-02.rb` demonstrates the main ractor sending messages to the spawned ractors and getting back a result, the `square` of the number passed as argument.

* `ractor-demo-03.rb` organizes the code of demo 2 with a class named `Actor`.

* `ractor-demo-04.rb` extracts the code running inside the ractor into a method of `Actor`.

* `ractor-demo-05.rb` adds a second method `cube` to `Actor` and demonstrates calls to both methods.

* `ractor-demo-06.rb` doesn't harcode in `Actor` the method that can run in ractor. The client can call any existing method and `Actor#method_missing` will find it and run it in a ractor.

* `ractor-demo-07.rb` moves `square` and `cube` in a subclass, named `MathActor` which also defined a mapping between the method names that a client can call and the internal implementations (`nn` for `square` and `nnn` for `cube`).

* `ractor-demo-08.rb` uses some metaprogramming magic to do without the mapping and the `nn` and `nnn` methods. The author of `MathActor` can define the methods that the client will call. It adds a module called `Action` that `MathActor` must use to define its methods that run in a ractor.

* `ractor-demo-09.rb` is the final cleanup of `Actor` and `Actions`.

* `ractor-demo-10.rb` is the example of the synopsys below.

# Synopsys

The intended use of `Actor` and `Actions` is

```
class MathActor < Actor
  extend Actions
  actions :mult

  # This runs inside a new ractor
  def mult(n, m)
    n * m
  end

  # This runs inside the same ractor of the caller
  def div(n, m)
    n / m
  end
end
```

The client code doesn't have to explicitly create a ractor.

```
a = MathActor.new
p a.mult(1, 2) # runs is new ractor
p a.div(2, 1) # runs in the main ractor
```

# Next steps

* Check what happens to the ractor when an actor gets out of scope.
* Experiment with ractors that implement a never ending loop of receiving inputs and returning results.
* Check what happens if `ActiveRecord` runs in `N` ractors: `N * pool size` connections?
* Possibly turn `Actor` and `Actions` into a gem.

# References

* <https://docs.ruby-lang.org/en/master/doc/ractor_md.html>
* <https://ruby-doc.org/core-3.0.0/Ractor.html>

# License

GNU Lesser General Public License version 3
<https://www.gnu.org/licenses/lgpl-3.0.html>
