#!/usr/bin/env ruby

# Same as demo 4 but calling two different methods of agent
# using Ractor.receive

require "etc"

class Agent
  def initialize(index)
    puts("Agent #{index} created")
    @ractor = Ractor.new(self) do |agent|
      agent.receiver
    end
  end
  def square(n)
    @ractor.send([:nn, n])
    @ractor.take
  end
  def cube(n)
    @ractor.send([:nnn, n])
    @ractor.take
  end
  def receiver
    method_name, arg = Ractor.receive
    send(method_name, arg)
  end
  def nn(n)
    n * n
  end
  def nnn(n)
    n * n * n
  end
end

processors = Etc.nprocessors
agents = (1..processors).map do |index|
  Agent.new(index)
end
results = agents.each_with_index.map {|agent, index| agent.square(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} = #{result}")
end

agents = (1..processors).map do |index|
  Agent.new(index)
end
results = agents.each_with_index.map {|agent, index| agent.cube(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} * #{index + 1} = #{result}")
end
puts "Quit"

# Output with 8 processors
# Agent 1 created
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# Agent 2 created
# Agent 3 created
# Agent 4 created
# Agent 5 created
# Agent 6 created
# Agent 7 created
# Agent 8 created
# 1 * 1 = 1
# 2 * 2 = 4
# 3 * 3 = 9
# 4 * 4 = 16
# 5 * 5 = 25
# 6 * 6 = 36
# 7 * 7 = 49
# 8 * 8 = 64
# Agent 1 created
# Agent 2 created
# Agent 3 created
# Agent 4 created
# Agent 5 created
# Agent 6 created
# Agent 7 created
# Agent 8 created
# 1 * 1 * 1 = 1
# 2 * 2 * 2 = 8
# 3 * 3 * 3 = 27
# 4 * 4 * 4 = 64
# 5 * 5 * 5 = 125
# 6 * 6 * 6 = 216
# 7 * 7 * 7 = 343
# 8 * 8 * 8 = 512
# Quit
