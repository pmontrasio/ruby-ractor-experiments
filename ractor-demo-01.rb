#!/usr/bin/env ruby

# A busy loop to check with htop how the interpreter saturates the CPUs
require "etc"
processors = Etc.nprocessors
puts "Making all #{processors} processors busy. Use htop to check the load on the machine."
ractors = (1..processors).map do |n|
  Ractor.new do
    index = receive
    puts("Ractor #{index} started")
    x = false
    while true
      x = !x
    end
  end
end
p ractors

ractors.each_with_index {|ractor, index| ractor.send(index + 1)}
sleep(10)
puts "Quit"

# Output with 8 processors
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# [#<Ractor:#2 ./ractor-demo.rb:5 running>, #<Ractor:#3 ./ractor-demo.rb:5 running>, #<Ractor:#4 ./ractor-demo.rb:5 running>, #<Ractor:#5 ./ractor-demo.rb:5 running>, #<Ractor:#6 ./ractor-demo.rb:5 running>, #<Ractor:#7 ./ractor-demo.rb:5 running>, #<Ractor:#8 ./ractor-demo.rb:5 blocking>, #<Ractor:#9 ./ractor-demo.rb:5 blocking>]
# Ractor 4 startedRactor 1 startedRactor 3 started

# Ractor 8 started
# Ractor 2 started

# Ractor 5 started
# Ractor 6 startedRactor 7 started

# Quit
