#!/usr/bin/env ruby

# Same as demo 7 but call the actor with the same method names implemented in the actor class.

# Problem to solve:
# The client calls actor.square and runs the square method defined in its own ractor but actor.square
# must run in another ractor.
# That method must be a wrapper that performs a Ractor#send to the ractor that implements MathActor
# which will call the original square method defined in MathActor.

# Solution:
# 1. The methods of MathActor that must run in the ractor are declared with actions :method1, :method2, ...
# 2. The initializer for Actor aliases those methods as action_method1, etc., to keep track of them.
# 3. The initializer creates the wrapper methods with the same name of the ones defined in MathActor
#    It prepends those methods to the ancestors chain of the class so they get called instead of the original ones.

# References about prepend and the ancestor chain:
# https://medium.com/@leo_hetsch/ruby-modules-include-vs-prepend-vs-extend-f09837a5b073
# https://stackoverflow.com/questions/4219277/how-do-i-wrap-the-invocation-of-a-ruby-method-by-including-a-module

require "etc"

class Actor
  def initialize(index)
    puts("Step 1. Actor #{index} created")
    methods = self.methods
    actions = self.class.const_get(:Action_names)
    methods_already_aliased = actions.map {|action| methods.include?(:"action_#{action}")}.uniq.include?(true)
    unless methods_already_aliased
      actions.each {|action| self.class.alias_method :"action_#{action}", action}
    end
    @ractor = Ractor.new(self) do |actor|
      actor.receiver
    end
    return if methods_already_aliased
    prepended_module = Module.new do
      actions.each do |action|
        define_method(action) do |*args|
          puts "6. Sending #{action}, #{args} to #{@ractor}"
          @ractor.send([:"action_#{action}", *args])
          @ractor.take
        end
      end
    end
    puts "2. Module #{prepended_module} created in #{Ractor.current}"
    actions.each do |action|
      puts "3. Method #{MathActor.instance_method(action)} created in #{Ractor.current}"
    end
    puts "4. #{Ractor.current} prepends #{prepended_module} with method square in #{Ractor.current}"
    self.class.prepend prepended_module
  end
  # This runs in a ractor
  def receiver
    method_name, *args = Ractor.receive
    puts "7. Received #{args} in #{Ractor.current}"
    puts "8. send #{args} to #{method_name} from #{Ractor.current}"
    send(method_name, *args)
  end
end

module Actions
  def actions(*action_names)
    self.const_set(:Action_names, action_names.freeze)
  end
end

class MathActor < Actor
  extend Actions
  actions :square, :cube
  def square(n)
    puts "square(#{n}) in #{Ractor.current}"
    n * n
  end
  def cube(n)
    puts "cube(#{n}) in #{Ractor.current}"
    n * n * n
  end
end

processors = Etc.nprocessors
range=(1..processors)
actors = range.map do |index|
  MathActor.new(index)
end
results = actors.each_with_index.map {|actor, index| actor.square(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} = #{result}")
end

actors = range.map do |index|
  MathActor.new(index)
end
results = actors.each_with_index.map {|actor, index| actor.cube(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} * #{index + 1} = #{result}")
end
puts "Quit"

# Output with 8 processors
# Step 1. Actor 1 created
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# 2. Module #<Module:0x0000560cdf437f00> created in #<Ractor:#1 running>
# 3. Method #<UnboundMethod: MathActor#square(args) ./ractor-demo-8.rb:79> created in #<Ractor:#1 running>
# 3. Method #<UnboundMethod: MathActor#cube(args) ./ractor-demo-8.rb:84> created in #<Ractor:#1 running>
# 4. #<Ractor:#1 running> prepends #<Module:0x0000560cdf437f00> with method square in #<Ractor:#1 running>
# Step 1. Actor 2 created
# Step 1. Actor 3 created
# Step 1. Actor 4 created
# Step 1. Actor 5 created
# Step 1. Actor 6 created
# Step 1. Actor 7 created
# Step 1. Actor 8 created
# 6. Sending square, [1] to #<Ractor:#2 ./ractor-demo-8.rb:39 blocking>
# 7. Received [1] in #<Ractor:#2 ./ractor-demo-8.rb:39 running>
# 8. send [1] to action_square from #<Ractor:#2 ./ractor-demo-8.rb:39 running>
# square(1) in #<Ractor:#2 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [2] to #<Ractor:#3 ./ractor-demo-8.rb:39 blocking>
# 7. Received [2] in #<Ractor:#3 ./ractor-demo-8.rb:39 running>
# 8. send [2] to action_square from #<Ractor:#3 ./ractor-demo-8.rb:39 running>
# square(2) in #<Ractor:#3 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [3] to #<Ractor:#4 ./ractor-demo-8.rb:39 blocking>
# 7. Received [3] in #<Ractor:#4 ./ractor-demo-8.rb:39 running>
# 8. send [3] to action_square from #<Ractor:#4 ./ractor-demo-8.rb:39 running>
# square(3) in #<Ractor:#4 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [4] to #<Ractor:#5 ./ractor-demo-8.rb:39 blocking>
# 7. Received [4] in #<Ractor:#5 ./ractor-demo-8.rb:39 running>
# 8. send [4] to action_square from #<Ractor:#5 ./ractor-demo-8.rb:39 running>
# square(4) in #<Ractor:#5 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [5] to #<Ractor:#6 ./ractor-demo-8.rb:39 blocking>
# 7. Received [5] in #<Ractor:#6 ./ractor-demo-8.rb:39 running>
# 8. send [5] to action_square from #<Ractor:#6 ./ractor-demo-8.rb:39 running>
# square(5) in #<Ractor:#6 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [6] to #<Ractor:#7 ./ractor-demo-8.rb:39 blocking>
# 7. Received [6] in #<Ractor:#7 ./ractor-demo-8.rb:39 running>
# 8. send [6] to action_square from #<Ractor:#7 ./ractor-demo-8.rb:39 running>
# square(6) in #<Ractor:#7 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [7] to #<Ractor:#8 ./ractor-demo-8.rb:39 blocking>
# 7. Received [7] in #<Ractor:#8 ./ractor-demo-8.rb:39 running>
# 8. send [7] to action_square from #<Ractor:#8 ./ractor-demo-8.rb:39 running>
# square(7) in #<Ractor:#8 ./ractor-demo-8.rb:39 running>
# 6. Sending square, [8] to #<Ractor:#9 ./ractor-demo-8.rb:39 blocking>
# 7. Received [8] in #<Ractor:#9 ./ractor-demo-8.rb:39 running>
# 8. send [8] to action_square from #<Ractor:#9 ./ractor-demo-8.rb:39 running>
# square(8) in #<Ractor:#9 ./ractor-demo-8.rb:39 running>
# 1 * 1 = 1
# 2 * 2 = 4
# 3 * 3 = 9
# 4 * 4 = 16
# 5 * 5 = 25
# 6 * 6 = 36
# 7 * 7 = 49
# 8 * 8 = 64
# Step 1. Actor 1 created
# Step 1. Actor 2 created
# Step 1. Actor 3 created
# Step 1. Actor 4 created
# Step 1. Actor 5 created
# Step 1. Actor 6 created
# Step 1. Actor 7 created
# Step 1. Actor 8 created
# 6. Sending cube, [1] to #<Ractor:#10 ./ractor-demo-8.rb:39 blocking>
# 7. Received [1] in #<Ractor:#10 ./ractor-demo-8.rb:39 running>
# 8. send [1] to action_cube from #<Ractor:#10 ./ractor-demo-8.rb:39 running>
# cube(1) in #<Ractor:#10 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [2] to #<Ractor:#11 ./ractor-demo-8.rb:39 blocking>
# 7. Received [2] in #<Ractor:#11 ./ractor-demo-8.rb:39 running>
# 8. send [2] to action_cube from #<Ractor:#11 ./ractor-demo-8.rb:39 running>
# cube(2) in #<Ractor:#11 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [3] to #<Ractor:#12 ./ractor-demo-8.rb:39 blocking>
# 7. Received [3] in #<Ractor:#12 ./ractor-demo-8.rb:39 running>
# 8. send [3] to action_cube from #<Ractor:#12 ./ractor-demo-8.rb:39 running>
# cube(3) in #<Ractor:#12 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [4] to #<Ractor:#13 ./ractor-demo-8.rb:39 blocking>
# 7. Received [4] in #<Ractor:#13 ./ractor-demo-8.rb:39 running>
# 8. send [4] to action_cube from #<Ractor:#13 ./ractor-demo-8.rb:39 running>
# cube(4) in #<Ractor:#13 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [5] to #<Ractor:#14 ./ractor-demo-8.rb:39 blocking>
# 7. Received [5] in #<Ractor:#14 ./ractor-demo-8.rb:39 running>
# 8. send [5] to action_cube from #<Ractor:#14 ./ractor-demo-8.rb:39 running>
# cube(5) in #<Ractor:#14 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [6] to #<Ractor:#15 ./ractor-demo-8.rb:39 blocking>
# 7. Received [6] in #<Ractor:#15 ./ractor-demo-8.rb:39 running>
# 8. send [6] to action_cube from #<Ractor:#15 ./ractor-demo-8.rb:39 running>
# cube(6) in #<Ractor:#15 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [7] to #<Ractor:#16 ./ractor-demo-8.rb:39 blocking>
# 7. Received [7] in #<Ractor:#16 ./ractor-demo-8.rb:39 running>
# 8. send [7] to action_cube from #<Ractor:#16 ./ractor-demo-8.rb:39 running>
# cube(7) in #<Ractor:#16 ./ractor-demo-8.rb:39 running>
# 6. Sending cube, [8] to #<Ractor:#17 ./ractor-demo-8.rb:39 blocking>
# 7. Received [8] in #<Ractor:#17 ./ractor-demo-8.rb:39 running>
# 8. send [8] to action_cube from #<Ractor:#17 ./ractor-demo-8.rb:39 running>
# cube(8) in #<Ractor:#17 ./ractor-demo-8.rb:39 running>
# 1 * 1 * 1 = 1
# 2 * 2 * 2 = 8
# 3 * 3 * 3 = 27
# 4 * 4 * 4 = 64
# 5 * 5 * 5 = 125
# 6 * 6 * 6 = 216
# 7 * 7 * 7 = 343
# 8 * 8 * 8 = 512
# Quit
