#!/usr/bin/env ruby

# Short lived Ractor to demonstrate receive and return values
require "etc"
processors = Etc.nprocessors
ractors = (1..processors).map do |n|
  Ractor.new do
    index = receive
    puts("Ractor #{index} started")
    index * index
  end
end
p ractors

ractors.each_with_index {|ractor, index| ractor.send(index + 1)}
ractors.each_with_index do |ractor, index|
  result = ractor.take
  puts("#{index + 1} * #{index + 1} = #{result}")
end
puts "Quit"

# Output with 8 processors
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# [#<Ractor:#2 ./ractor-demo-2.rb:5 running>, #<Ractor:#3 ./ractor-demo-2.rb:5 running>, #<Ractor:#4 ./ractor-demo-2.rb:5 running>, #<Ractor:#5 ./ractor-demo-2.rb:5 running>, #<Ractor:#6 ./ractor-demo-2.rb:5 running>, #<Ractor:#7 ./ractor-demo-2.rb:5 running>, #<Ractor:#8 ./ractor-demo-2.rb:5 blocking>, #<Ractor:#9 ./ractor-demo-2.rb:5 blocking>]
# Ractor 2 started
# Ractor 5 started
# Ractor 1 startedRactor 6 started

# 1 * 1 = 1
# 2 * 2 = 4
# Ractor 4 startedRactor 3 started

# 3 * 3 = 9
# Ractor 8 started
# 4 * 4 = 16
# 5 * 5 = 25
# 6 * 6 = 36
# Ractor 7 started
# 7 * 7 = 49
# 8 * 8 = 64
# Quit
