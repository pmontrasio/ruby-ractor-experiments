#!/usr/bin/env ruby

# Same as demo 6 but with a subclass inheriting the Agent infrastructure
# using Ractor.receive
# Change 2: rename Agent into Actor

require "etc"

class Actor
  def initialize(index)
    puts("Actor #{index} created")
    @ractor = Ractor.new(self) do |actor|
      actor.receiver
    end
  end
  def method_missing(method_name, *args)
    raise NoMethodError(method_name) unless actor_actions.include?(method_name)
    method = actor_actions[method_name]
    @ractor.send([method, *args])
    @ractor.take
  end
  def receiver
    method_name, *args = Ractor.receive
    puts "Call #{MathActor.instance_method(method_name)} in #{Ractor.current}"
    send(method_name, *args)
  end
end

class MathActor < Actor
  def actor_actions
    {
      square: :nn,
      cube: :nnn
    }
  end
  def nn(n)
    n * n
  end
  def nnn(n)
    n * n * n
  end
end

processors = Etc.nprocessors
actors = (1..processors).map do |index|
  MathActor.new(index)
end
results = actors.each_with_index.map {|actor, index| actor.square(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} = #{result}")
end

actors = (1..processors).map do |index|
  MathActor.new(index)
end
results = actors.each_with_index.map {|actor, index| actor.cube(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} * #{index + 1} = #{result}")
end
puts "Quit"

# Output with 8 processors
# Actor 1 created
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# Actor 2 created
# Actor 3 created
# Actor 4 created
# Actor 5 created
# Actor 6 created
# Actor 7 created
# Actor 8 created
# 1 * 1 = 1
# 2 * 2 = 4
# 3 * 3 = 9
# 4 * 4 = 16
# 5 * 5 = 25
# 6 * 6 = 36
# 7 * 7 = 49
# 8 * 8 = 64
# Actor 1 created
# Actor 2 created
# Actor 3 created
# Actor 4 created
# Actor 5 created
# Actor 6 created
# Actor 7 created
# Actor 8 created
# 1 * 1 * 1 = 1
# 2 * 2 * 2 = 8
# 3 * 3 * 3 = 27
# 4 * 4 * 4 = 64
# 5 * 5 * 5 = 125
# 6 * 6 * 6 = 216
# 7 * 7 * 7 = 343
# 8 * 8 * 8 = 512
# Quit
