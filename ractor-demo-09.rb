#!/usr/bin/env ruby

# Same as demo 8 with a demonstration of using public and private methods in MathActor.
# Also experiment Ruby's 3 endless method definition.
# Remove the puts used for tracing the inner workings of Actor.

require "etc"

class Actor
  def initialize
    methods = self.methods
    actions = self.class.const_get(:Action_names)
    methods_already_aliased = actions.map {|action| methods.include?(:"action_#{action}")}.uniq.include?(true)
    unless methods_already_aliased
      actions.each {|action| self.class.alias_method :"action_#{action}", action}
    end
    @ractor = Ractor.new(self) do |actor|
      actor.receiver
    end
    return if methods_already_aliased
    prepended_module = Module.new do
      actions.each do |action|
        define_method(action) do |*args|
          @ractor.send([:"action_#{action}", *args])
          @ractor.take
        end
      end
    end
    self.class.prepend prepended_module
  end
  # This runs in a ractor
  def receiver
    method_name, *args = Ractor.receive
    send(method_name, *args)
  end
end

module Actions
  def actions(*action_names)
    self.const_set(:Action_names, action_names.freeze)
  end
end

class MathActor < Actor
  extend Actions
  actions :square, :cube

  def square(n) = nn(identity(n))
  def cube(n) = nnn(identity(n))

  def identity(n) = n
  private
  def nn(n) = n * n
  def nnn(n) = n * n * n
end

processors = Etc.nprocessors
actor_numbers=(1..processors)
actors = actor_numbers.map {|_| MathActor.new}
results = actors.each_with_index.map {|actor, index| actor.square(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} = #{result}")
end

actors = actor_numbers.map {|_| MathActor.new }
results = actors.each_with_index.map {|actor, index| actor.cube(index + 1)}
results.each_with_index.map do |result, index|
  puts("#{index + 1} * #{index + 1} * #{index + 1} = #{result}")
end
puts "Quit"

# Output with 8 processors
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# 1 * 1 = 1
# 2 * 2 = 4
# 3 * 3 = 9
# 4 * 4 = 16
# 5 * 5 = 25
# 6 * 6 = 36
# 7 * 7 = 49
# 8 * 8 = 64
# 1 * 1 * 1 = 1
# 2 * 2 * 2 = 8
# 3 * 3 * 3 = 27
# 4 * 4 * 4 = 64
# 5 * 5 * 5 = 125
# 6 * 6 * 6 = 216
# 7 * 7 * 7 = 343
# 8 * 8 * 8 = 512
# Quit
