#!/usr/bin/env ruby

class Actor
  def initialize
    methods = self.methods
    actions = self.class.const_get(:Action_names)
    methods_already_aliased = actions.map {|action| methods.include?(:"action_#{action}")}.uniq.include?(true)
    unless methods_already_aliased
      actions.each {|action| self.class.alias_method :"action_#{action}", action}
    end
    @ractor = Ractor.new(self) do |actor|
      actor.receiver
    end
    return if methods_already_aliased
    prepended_module = Module.new do
      actions.each do |action|
        define_method(action) do |*args|
          @ractor.send([:"action_#{action}", *args])
          @ractor.take
        end
      end
    end
    self.class.prepend prepended_module
  end
  # This runs in a ractor
  def receiver
    method_name, *args = Ractor.receive
    send(method_name, *args)
  end
end

module Actions
  def actions(*action_names)
    self.const_set(:Action_names, action_names.freeze)
  end
end

class MathActor < Actor
  extend Actions
  actions :mult

  # This runs inside a new ractor
  def mult(n, m)
    p "mult in #{Ractor.current}"
    n * m
  end

  # This runs inside the same ractor of the caller
  def div(n, m)
    p "div in #{Ractor.current}"
    n / m
  end
end

a = MathActor.new
p "main ractor #{Ractor.current}"
p a.mult(1, 2) # runs is new ractor
p a.div(2, 1) # runs in the main ractor

# Output
# <internal:ractor>:267: warning: Ractor is experimental, and the behavior may change in future versions of Ruby! Also there are many implementation issues.
# "main ractor #<Ractor:#1 running>"
# "mult in #<Ractor:#2 ./ractor-demo-10.rb:11 running>"
# 2
# "div in #<Ractor:#1 running>"
# 2
